"use strict";

// task 1

const product = {
    name: "Футболка",
    price: 100, 
    discount: 20, 

    calculateTotalPrice: function() {
        const discountedPrice = this.price * (1 - this.discount / 100);
        return discountedPrice;
    }
};

const totalPrice = product.calculateTotalPrice();
console.log(`Повна ціна товару ${product.name} з урахуванням знижки: ${totalPrice} грн.`);

// task 2

// function greetUser(user) {
//     return `Привіт, мені ${user.age} років!`;
// }

// const userName = prompt("Введіть своє ім'я:");
// const userAge = parseFloat(prompt("Введіть свій вік:"));

// const user = {
//     name: userName,
//     age: userAge
// };

// const greeting = greetUser(user);
// alert(greeting);

// task 3

// function deepClone(obj) {
//     if (obj === null || typeof obj !== 'object') {
//         return obj;
//     }

//     if (Array.isArray(obj)) {
//         const newArray = [];
//         for (let i = 0; i < obj.length; i++) {
//             newArray[i] = deepClone(obj[i]);
//         }
//         return newArray;
//     }

//     const newObj = {};
//     for (const key in obj) {
//         if (obj.hasOwnProperty(key)) {
//             newObj[key] = deepClone(obj[key]);
//         }
//     }
//     return newObj;
// }

// // Example usage:
// const originalObject = {
//     name: 'John',
//     age: 30,
//     address: {
//         city: 'New York',
//         country: 'USA'
//     },
//     hobbies: ['reading', 'swimming']
// };

// const clonedObject = deepClone(originalObject);
// console.log(clonedObject);


